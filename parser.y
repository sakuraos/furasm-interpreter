%{
#include <stdio.h>
int yylex(void);
int yyerror(const char*);
%}

%%
input:
     %empty;

%%

int yyerror(const char* str){
fprintf(stderr, "%s\n", str);
return 1;
}
