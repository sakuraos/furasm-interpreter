# FurASM Interpreter

## Syntax
- A line marks an instruction if it is not a comment or macro.
- Instruction arguments are separated by whitespace.
- Instruction arguments can only be register identifiers or int literals.
- A line marks a comment if it starts with an at sign (@).
- If a macro contains an equals sign (=), any text after it and before a newline (\n) is considered a macro value.
- Anything after a semicolon (;) is considered a comment.

## Registers
- OWO
- UWU
- ONO
- UNU
- MEW (meta-register)
    Values assigned to this meta-register will be printed to the console as an ASCII character.
    Using this meta-register as an instruction argument will prompt that an optional number be requested from the console.
- DMW (meta-register)
    Values assigned to this meta-register will be printed to the console.
    Using this meta-register as an instruction argument will prompt that a required number be requested from the console.

## Instructions
### pet
Arguments [2]: register, value

Description: Sets <register> to <value>

### paw
Arguments [2]: register, value

Description: Adds <register> and <value>

### bop
Arguments [2]: register, value

Description: Subtracts <register> and <value>

### lik
Arguments [2]: register, value

Description: Multiplies <register> and <value>

### kis
Arguments [2]: register, value

Description: Divides <register> and <value>

### bte
Arguments [2]: register, value

Description: Modulos <register> and <value>

### cyt
Arguments [3]: register, value1, value2

Description: Sets <register> to 0 if value1 is greater than value2

### wag
Arguments [3]: register, value1, value2

Description: Sets <register> to 0 if value1 is equal to value2

### pnc
Arguments [1]: pointer

Description: Pushes the pointer of the next instruction to the stack and jumps to <pointer>

### wig
Arguments [1]: pointer

Description: Jumps to <pointer>

### nuz
Description: Pops stack and jumps to the returned pointer

### pat
Arguments [1]: register

Description: Skips next instruction if <register> is 0

### yif
Description: Terminate the program.
